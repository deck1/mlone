# %%
import pandas as pd
import numpy as np
import boto3
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from numpy import arange
from sklearn import tree
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_validate
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from fpdf import FPDF 
import seaborn as sns
from sklearn.pipeline import Pipeline
# import pickle
import io
import pickle5 as pickle
import datetime
from datetime import datetime as dt
from datetime import timedelta

# %%
pd.options.display.max_columns = None
pd.options.display.max_rows = None

# %%
s3 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com")
print("----load start------")
data=s3.Object('upload-zszljfwd','loan_f10.pickle').get()['Body'].read()
df_raw=pd.read_pickle(io.BytesIO(data),compression=None)
print("---load done----")

# %%
df=df_raw.copy()

# %%
df.shape

# %%
df.ZZ_STATUS.value_counts()

# %%
X = df.drop('ZZ_STATUS', axis=1)
y = df['ZZ_STATUS'].values

# %%
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.20, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train,y_train,test_size=0.25, random_state=0)

# %%
sm = SMOTE()

# %%
X_train_smote, y_train_smote = sm.fit_resample(X_train, y_train)

# %%
def build_logistic_regression():
    print("Starting ------ Logistic Regression")
    start_ts=datetime.datetime.now() 
    pipe = Pipeline(steps=[
        ('logistic', LogisticRegression())
    ])
    
    param_grid ={
        'logistic__penalty':('l1', 'l2', 'elasticnet', 'none'),
        'logistic__solver':('newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'),
        'logistic__max_iter':[500,1000,2000,5000,10000]
    }
    model=GridSearchCV(estimator=pipe,
                             param_grid=param_grid,
                             scoring='roc_auc', 
#                              n_jobs=3,
                             pre_dispatch='2*n_jobs', 
                             cv=2, 
                             verbose=1,
                             return_train_score=False)
    print("model fit")
    model.fit(X_train_smote,y_train_smote)
    pkl_filename = "./model/logistic_regression.pkl"
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)

    X_val_np = X_val.to_numpy()
    predicted = model.predict(X_val_np)
    proba = model.predict_proba(X_val_np)
    
    precision=precision_score(y_val, predicted, average='weighted')
    recall=recall_score(y_val, predicted, average='weighted')
    f1=f1_score(y_val, predicted, average='weighted')
    accuracy=accuracy_score(y_val, predicted)
    
    CM = confusion_matrix(y_val, predicted)
    (TN,FN,TP,FP) = (CM[0][0],CM[1][0],CM[1][1],CM[0][1])
    FPR = FP/(FP+TN)
    
    end_ts=datetime.datetime.now()
    delta=(end_ts-start_ts)
    
    COLUMNS=['Algorithm','Accuracy','Precision','Recall','F1-support','FPR','RUNTIME','PROBA','PREDICT','ESTIMATOR','BEST PARAMS']
    dic=[['Logistic Regression',accuracy,precision,recall,f1,FPR,str(delta),proba,predicted,model.best_estimator_,model.best_params_]]
    df=pd.DataFrame(dic,columns=COLUMNS)
    print("Finishing ------ Logistic Regression")
    return df

# %%
def build_profile_summary(df):
    print("REPORTING")
    pdf = FPDF(format='A4')
    df_new=df
    df_new=df_new.drop(['PROBA'], axis=1)
    pdf.add_page()
    pdf.set_font("Arial", size=20)
    pdf.cell(200, 10, txt="Build Profile Summary",  border = 0,ln=1, align="C")
    pdf.cell(ln=1, h=5.0, align='L', w=0, txt='', border=0)
    pdf.set_font("Arial", 'B', size=9)
    pdf.cell(w=40, h=6.0, txt="Algorithm", border=1, align='C')
    pdf.cell(w=20, h=6.0, txt="Accuracy", border=1, align='C')
    pdf.cell(w=20, h=6.0, txt="Precision", border=1, align='C')
    pdf.cell(w=20, h=6.0, txt="Recall", border=1, align='C')
    pdf.cell(w=20, h=6.0, txt="F1-Score", border=1, align='C')
    pdf.cell(w=20, h=6.0, txt="FPR", border=1, align='C')
    pdf.cell(w=40, h=6.0, txt="RUNTIME", border=1, align='C')
    pdf.ln(7)
    pdf.set_font("Arial", size=7)
    for index, row in df_new.iterrows(): 
        pdf.cell(w=40, h=6.0, txt=row['Algorithm'], border=1, align='C')
        pdf.cell(w=20, h=6.0, txt=str(round(row['Accuracy']*100,2))+"%", border=1, align='C')
        pdf.cell(w=20, h=6.0, txt=str(round(row['Precision']*100,2))+"%", border=1, align='C')
        pdf.cell(w=20, h=6.0, txt=str(round(row['Recall']*100,2))+"%", border=1, align='C')
        pdf.cell(w=20, h=6.0, txt=str(round(row['F1-support']*100,2))+"%", border=1, align='C')
        pdf.cell(w=20, h=6.0, txt=str(round(row['FPR']*100,2))+"%", border=1, align='C')
        pdf.cell(w=40, h=6.0, txt=row['RUNTIME'], border=1, align='C')
        pdf.ln(7)
        pass
    
    for index1, row1 in df.iterrows():
        pdf.add_page()
        pdf.set_font("Arial", size=20)
        pdf.cell(200, 10, txt=row1['Algorithm'],  border = 0,ln=1, align="C")
        pdf.cell(ln=1, h=5.0, align='L', w=0, txt='', border=0)
        fpr, tpr, thresholds = roc_curve(y_val,row1['PROBA'][:, 1])
        auc = roc_auc_score(y_val, row1['PROBA'][:, 1])
        plt.figure(figsize=(4,4))
        plt.plot(fpr, tpr, label=row1['Algorithm']+'(area = %0.2f)' % auc)
        plt.plot([0, 1], [0, 1],'r--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        di='./model/'+row1['Algorithm']
        plt.savefig(di)
        plt.clf()
        
        di_new=di+'.png'
        pdf.image(di_new)
        pdf.ln(7)
        pdf.set_font("Arial", size=9)

        pdf.cell(w=40, h=6.0, txt='Accuracy', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=str(round(row1['Accuracy']*100,2))+"%", border=1, align='L') 
        pdf.cell(w=30, h=6.0, txt='', border=0, align='L')
        pdf.ln(5)
        st=[]
        for keys in row1['BEST PARAMS']:
            nw_st=''
            nw_st=nw_st+keys + " : "
            nw_st=nw_st+str(row1['BEST PARAMS'][keys])
            
            st.append(nw_st)
            pass
        for line in st:
            pdf.cell(w=40, h=6.0, txt='', border=0, align='L')
            pdf.cell(w=40, h=6.0, txt='', border=0, align='L') 
            pdf.cell(w=30, h=6.0, txt='', border=0, align='L')
            pdf.cell(w=40, h=6.0, txt=line, border=1, align='L')
            pdf.ln(5)
            pass
        pdf.cell(w=40, h=6.0, txt='Precision', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=str(round(row1['Precision']*100,2))+"%", border=1, align='L')
        pdf.ln(5)
        pdf.cell(w=40, h=6.0, txt='Recall', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=str(round(row1['Recall']*100,2))+"%", border=1, align='L')
        pdf.ln(5)
        pdf.cell(w=40, h=6.0, txt='F1-score', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=str(round(row1['F1-support']*100,2))+"%", border=1, align='L')
        pdf.ln(5)
        pdf.cell(w=40, h=6.0, txt='FPR', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=str(round(row1['FPR']*100,2))+"%", border=1, align='L')   
        pdf.ln(5)
        pdf.cell(w=40, h=6.0, txt='RUNTIME', border=1, align='L')
        pdf.cell(w=40, h=6.0, txt=row1['RUNTIME'], border=1, align='L') 
        confusion_mat = confusion_matrix(y_val, row1['PREDICT'])
        confusion_df = pd.DataFrame(confusion_mat, index=['Actual N','Actual P'], columns=['Predicted N','Predicted P'])
        di2='./model/'+row1['Algorithm']+'_confusion_heat'
        sns_plot = sns.heatmap(confusion_df, cmap='coolwarm', annot=True,fmt="d")
        figure = sns_plot.get_figure()  
        figure.savefig(di2)
        di2_new=di2+'.png'
        pass
    
    file_dir='./model/build_profile_'+str(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))+'.pdf' 
    pdf.output(file_dir, 'F')
    print("REPORTING FINISH")
    return 1

# %%
if __name__ == '__main__':
    df=build_logistic_regression()
    build_profile_summary(df)

# %%
    